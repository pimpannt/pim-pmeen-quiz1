package service;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;




@SpringBootTest(classes = CheckService.class)
public class CheckServiceTest {

    @Autowired
    private CheckService checkService;

    @Test
    public void checkValidString_test(){
        Assertions.assertEquals("Group A",checkService.checkValidString("HELLO5555BBBCCCTEST"));
        Assertions.assertNull(checkService.checkValidString("HELLO54545454545Test"));
        Assertions.assertEquals("Group A",checkService.checkValidString("APIxxx88fddfdfjjdsfdsfEOF"));
        Assertions.assertEquals("Group A",checkService.checkValidString("APIxxx88fddfdfMEENMEENjjdsfdsfEOF"));
        Assertions.assertEquals("Group A",checkService.checkValidString("THIS.IS.SOME.TEST.NAND.STRING.EOF"));
        Assertions.assertEquals("Group B",checkService.checkValidString("AABBCC.HELLO.00"));
    }
}
