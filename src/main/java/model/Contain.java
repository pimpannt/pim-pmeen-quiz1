package model;

public class Contain {

    private String containWord;
    private Boolean isCaseSenSitive;

    public Contain(String containWord, Boolean isCaseSenSitive) {
        this.containWord = containWord;
        this.isCaseSenSitive = isCaseSenSitive;
    }

    public String getContainWord() {
        return containWord;
    }

    public void setContainWord(String containWord) {
        this.containWord = containWord;
    }

    public Boolean getCaseSenSitive() {
        return isCaseSenSitive;
    }

    public void setCaseSenSitive(Boolean caseSenSitive) {
        isCaseSenSitive = caseSenSitive;
    }
}
