package model;

public class StartWith {

    private String startWord;
    private Boolean isCaseSenSitive;


    public StartWith(String startWord, Boolean isCaseSenSitive) {
        this.startWord = startWord;
        this.isCaseSenSitive = isCaseSenSitive;
    }

    public String getStartWord() {
        return startWord;
    }

    public void setStartWord(String startWord) {
        this.startWord = startWord;
    }

    public Boolean getCaseSenSitive() {
        return isCaseSenSitive;
    }

    public void setCaseSenSitive(Boolean caseSenSitive) {
        isCaseSenSitive = caseSenSitive;
    }
}
