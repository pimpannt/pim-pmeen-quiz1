package model;

import java.util.List;

public class Rule {

    private String ruleName;
    private StartWith startWith;
    private List<Contain> containWith;
    private EndWith endWith;
    private String groupName;

    public Rule(String ruleName, StartWith startWith, List<Contain> containWith, EndWith endWith){
        this.ruleName = ruleName;
        this.startWith = startWith;
        this.containWith = containWith;
        this.endWith = endWith;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public StartWith getStartWith() {
        return startWith;
    }

    public void setStartWith(StartWith startWith) {
        this.startWith = startWith;
    }

    public List<Contain> getContainWith() {
        return containWith;
    }

    public void setContainWith(List<Contain> containWith) {
        this.containWith = containWith;
    }

    public EndWith getEndWith() {
        return endWith;
    }

    public void setEndWith(EndWith endWith) {
        this.endWith = endWith;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
