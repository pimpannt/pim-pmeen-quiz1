package model;

public class EndWith {

    private String endWord;
    private Boolean isCaseSenSitive;

    public EndWith(String endWord, Boolean isCaseSenSitive) {
        this.endWord = endWord;
        this.isCaseSenSitive = isCaseSenSitive;
    }

    public String getEndWord() {
        return endWord;
    }

    public void setEndWord(String endWord) {
        this.endWord = endWord;
    }

    public Boolean getCaseSenSitive() {
        return isCaseSenSitive;
    }

    public void setCaseSenSitive(Boolean caseSenSitive) {
        isCaseSenSitive = caseSenSitive;
    }
}
