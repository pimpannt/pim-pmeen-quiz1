package service;

import model.Contain;
import model.EndWith;
import model.Rule;
import model.StartWith;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class CheckService {

    /**Please Create Table name "RULE_CONFIG" table with following schema
     * RuleName (PK Unique not null) VARCHAR(10)
     * Rule (nullable) NVARCHAR(MAX) --- Please note that we will save rule as a JSON String ---
     * GroupName (nullable) VARCHAR(10)
     *
     * Sample value
     * RuleName => "rule#A1"
     * Rule => "{"startWith":{"word":"HELLO","isCaseSensitive":true},"containWith":[{"word":"TEST","isCaseSensitive":true}],"endWith":null}"
     * GroupName => "A"
     *
     * RuleName => "rule#B2"
     * Rule => "{"startWith":{"word":"PROMPT","isCaseSensitive":false},"containWith":[{"word":"APIFF","isCaseSensitive":false}
        ,{"word":"MEENZA","isCaseSensitive":false},{"word":"NANDZA","isCaseSensitive":true}],"endWith":{"word":"00","isCaseSensitive":true}}"
     * GroupName => "B"
     *
     */

    private final String groupA = "Group A";
    private final String groupB = "Group B";


    //These records retrieved from database



    private List<Rule> getListOfGroupARules(){

        //TODO: Convert Rule into Entity that read record RULE_CONFIG
        // Replace below lines with List<Rule> groupBRule = JPARepository.findByGroupName("A");

         List<Rule> groupARule = Arrays.asList(
                new Rule("rule#A1"
                        ,new StartWith("HELLO",true)
                        ,Arrays.asList(new Contain("TEST",true))
                        ,null)
                ,new Rule("rule#A2"
                        ,new StartWith("API",true)
                        ,null
                        ,new EndWith("EOF",false))
                ,new Rule("rule#A3"
                        ,null
                        ,Arrays.asList(new Contain("MEENMEEN",false))
                        ,null)
                ,new Rule("rule#A4a"
                        ,null
                        ,Arrays.asList(new Contain("NAND",false))
                        ,new EndWith("EOF",false))
                ,new Rule("rule#A4b"
                        ,null
                        ,Arrays.asList(new Contain("PATPAT",false))
                        ,new EndWith("EOF",false))
        );
       return groupARule;
    }

    private List<Rule> getListOfGroupBRules() {

        //TODO: Convert Rule into Entity that read record RULE_CONFIG
        // Replace below lines with List<Rule> groupBRule = JPARepository.findByGroupName("B");

        List<Rule> groupBRule = Arrays.asList(
                new Rule("rule#B1"
                        , new StartWith("PROMPT", false)
                        , null
                        , null)
                , new Rule("rule#B2"
                        , new StartWith("PROMPT", false)
                        , Arrays.asList(
                            new Contain("APIFF", false)
                            , new Contain("MEENZA", false)
                            , new Contain("NANDZA", true))
                        , new EndWith("00", true))
                , new Rule("rule#B3a"
                        , new StartWith("ABC", true)
                        , null
                        , new EndWith("00", true))
                , new Rule("rule#B3b"
                        , new StartWith("AABBCC", true)
                        , null
                        , new EndWith("00", true))

        );
        return groupBRule;
    }

    public String checkValidString(String str){

        System.out.println("------------------------------------");
        System.out.println("checkValidString of string..."+str);

        //Check for group A
        for(Rule ruleGroupA : getListOfGroupARules()){
            System.out.println("[GroupA] checkValidString of with rule..."+ruleGroupA.getRuleName());
            if(checkValidStringForGroupA(ruleGroupA,str)) {
                return  groupA;
            }
        }

        //Check for group B
        for(Rule ruleGroupB : getListOfGroupBRules()){
            System.out.println("[GroupB] checkValidString of with rule..."+ruleGroupB.getRuleName());
            if(checkValidStringForGroupB(ruleGroupB,str)) {
                return  groupB;
            }
        }

        return null;
    }


    private Boolean checkValidStringForGroupA(Rule ruleGroupA,String str){

        if((CheckStartWith(ruleGroupA,str) && CheckContainWith(ruleGroupA,str)) //rule#A1
                ||(CheckStartWith(ruleGroupA,str) && CheckEndWith(ruleGroupA,str)) //rule#A2
                ||(ruleGroupA.getStartWith() == null && ruleGroupA.getEndWith() == null && CheckContainWith(ruleGroupA,str)) //rule#A3
                ||(CheckContainWith(ruleGroupA,str) && CheckEndWith(ruleGroupA,str)) //rule#A4a and rule#A4b
        ) {
            return true;
        }
        return false;
    }

    public Boolean checkValidStringForGroupB(Rule ruleGroupB,String str){

        if(CheckStartWith(ruleGroupB,str) //rule#B1
                || (CheckContainWith(ruleGroupB,str) && CheckEndWith(ruleGroupB,str)) //rule#B2
                || (CheckStartWith(ruleGroupB,str) && CheckEndWith(ruleGroupB,str)) //rule#B3a and rule#Bb
        ){
            return true;
        }
        return false;
    }



    public Boolean CheckStartWith(Rule rule,String str){

        if(rule.getStartWith() != null){
            if(!rule.getStartWith().getCaseSenSitive()){
                return str.toUpperCase().startsWith(rule.getStartWith().getStartWord().toUpperCase());
            } else {
                return str.startsWith(rule.getStartWith().getStartWord());
            }
        }
        return false;
    }

    public Boolean CheckEndWith(Rule rule,String str){

        if(rule.getEndWith() != null){
            if(!rule.getEndWith().getCaseSenSitive()){
                return str.toUpperCase().endsWith(rule.getEndWith().getEndWord().toUpperCase());
            } else {
                return str.endsWith(rule.getEndWith().getEndWord());
            }
        }
        return false;
    }


    public Boolean CheckContainWith(Rule rule,String str){

        if(rule.getContainWith() != null){
            for(Contain contain : rule.getContainWith()){
                if(!contain.getCaseSenSitive()){
                    return str.toUpperCase().contains(contain.getContainWord().toUpperCase());
                } else {
                    return str.contains(contain.getContainWord());
                }
            }
        }
        return false;
    }


}
